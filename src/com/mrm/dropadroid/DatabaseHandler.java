package com.mrm.dropadroid;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "dropanote";

	// table names
	private static final String TABLE_BOOKS = "books";
	private static final String TABLE_ARTICLES = "articles";

	// Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_TITLE = "title";
	private static final String KEY_CREATED = "created";
	private static final String KEY_MODIFIED = "modified";

	private static final String KEY_BOOKID = "book_id";
	private static final String KEY_TEXT = "text";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_BOOKS_TABLE = "CREATE TABLE " + TABLE_BOOKS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TITLE
				+ " TEXT," + KEY_CREATED
				+ " DATETIME default current_timestamp," + KEY_MODIFIED
				+ " DATETIME default current_timestamp" + ")";
		String CREATE_ARTICLES_TABLE = "CREATE TABLE " + TABLE_ARTICLES + "("
				+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_BOOKID
				+ " INTEGER," + KEY_TITLE + " TEXT," + KEY_TEXT + " TEXT,"
				+ KEY_CREATED + " DATETIME default current_timestamp,"
				+ KEY_MODIFIED + " DATETIME default current_timestamp" + ")";
		db.execSQL(CREATE_BOOKS_TABLE);
		db.execSQL(CREATE_ARTICLES_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKS);

		// Create tables again
		onCreate(db);
	}

	/**
	 * BOOKS CRUD METHODS
	 */
	// Adding new Book
	public void addBook(Book book) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_TITLE, book.getTitle());
		db.insert(TABLE_BOOKS, null, values);
		db.close();
	}

	// Getting single Book
	public Book getBook(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_BOOKS,
				new String[] { KEY_ID, KEY_TITLE }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}

		Book book = new Book(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2), cursor.getString(3));
		// return book
		return book;
	}

	// Getting All Books
	public List<Book> getAllBooks() {
		List<Book> bookList = new ArrayList<Book>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_BOOKS + " order by " + KEY_CREATED + " desc";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Book book = new Book();
				book.setID(Integer.parseInt(cursor.getString(0)));
				book.setTitle(cursor.getString(1));
				book.setCreated(cursor.getString(2));
				// book.setModified(Integer.parseInt(cursor.getString(3)));
				// Adding book to list
				bookList.add(book);
			} while (cursor.moveToNext());
		}

		// return book list
		db.close();
		return bookList;
	}

	// Getting Books Count
	public int getBookCount() {
		String countQuery = "SELECT  * FROM " + TABLE_BOOKS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		db.close();

		// return count
		return cursor.getCount();
	}

	// Updating single Book
	public int updateBook(Book book) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TITLE, book.getTitle());
		// TODO: CREATE MODIFIED TIMESTAMP
	    Calendar calendar = Calendar.getInstance();
	    java.sql.Timestamp timestamp = new java.sql.Timestamp(calendar.getTime().getTime());
		values.put(KEY_MODIFIED, timestamp.toString());

		// updating row
		int retval = db.update(TABLE_BOOKS, values, KEY_ID + " = ?",
				new String[] { String.valueOf(book.getID()) });
		db.close();
		return retval;

	}

	// Deleting single Book
	public void deleteBook(Book book) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_ARTICLES, KEY_BOOKID + " = ?",
				new String[] { String.valueOf(book.getID()) });
		db.delete(TABLE_BOOKS, KEY_ID + " = ?",
				new String[] { String.valueOf(book.getID()) });
		db.close();
	}

	/**
	 * ARTICLES CRUD METHODS
	 */
	// Adding new Article
	public int addArticle(Article article) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_TITLE, article.getTitle());
		values.put(KEY_TEXT, article.getText());
		values.put(KEY_BOOKID, article.getBookID());
		System.out.println("Inbserting: " + values.toString());
		int retval = new BigDecimal(db.insert(TABLE_ARTICLES, null, values)).intValueExact();
		db.close();
		return retval;
	}

	// Getting single Article
	public Article getArticle(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_ARTICLES, new String[] { KEY_ID,
				KEY_BOOKID, KEY_TITLE, KEY_TEXT,KEY_CREATED, KEY_MODIFIED}, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}

		Article article = new Article(
				Integer.parseInt(cursor.getString(0)),
				Integer.parseInt(cursor.getString(1)), 
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getString(4), 
				cursor.getString(5));
		// return article
		return article;
	}

	// Getting All Articles
	public List<Article> getAllArticles() {
		List<Article> articleList = new ArrayList<Article>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ARTICLES;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Article article = new Article();
				article.setID(Integer.parseInt(cursor.getString(0)));
				article.setBookID(Integer.parseInt(cursor.getString(1)));
				article.setTitle(cursor.getString(2));
				article.setText(cursor.getString(3));
				article.setCreated(cursor.getString(4));
				article.setModified(cursor.getString(5));
				// Adding article to list
				articleList.add(article);
			} while (cursor.moveToNext());
		}

		// return Article list
		db.close();
		return articleList;
	}
	
	// Getting All Articles
	public List<Article> getArticlesForBook(int bookId) {
		List<Article> articleList = new ArrayList<Article>();
		// Select All Query

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(TABLE_ARTICLES, new String[] { KEY_ID,
				KEY_BOOKID, KEY_TITLE, KEY_TEXT }, KEY_BOOKID + "=?",
				new String[] { String.valueOf(bookId) }, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Article article = new Article();
				article.setID(Integer.parseInt(cursor.getString(0)));
				article.setBookID(Integer.parseInt(cursor.getString(1)));
				article.setTitle(cursor.getString(2));
				article.setText(cursor.getString(3));
				// Adding article to list
				articleList.add(article);
			} while (cursor.moveToNext());
		}

		// return Article list
		db.close();
		return articleList;
	}

	// Getting Articles Count
	public int getArticleCount() {
		String countQuery = "SELECT  * FROM " + TABLE_ARTICLES;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		db.close();

		// return count
		return cursor.getCount();
	}

	// Updating single Article
	public int updateArticle(Article article) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TITLE, article.getTitle());
		values.put(KEY_TEXT, article.getText());
		// TODO: CREATE MODIFIED TIMESTAMP
	    Calendar calendar = Calendar.getInstance();
	    java.sql.Timestamp timestamp = new java.sql.Timestamp(calendar.getTime().getTime());
		values.put(KEY_MODIFIED, timestamp.toString());

		// updating row
		int retval = db.update(TABLE_ARTICLES, values, KEY_ID + " = ?",
				new String[] { String.valueOf(article.getID()) });
		db.close();
		return retval;

	}

	// Deleting single Article
	public void deleteArticle(Article article) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_ARTICLES, KEY_ID + " = ?",
				new String[] { String.valueOf(article.getID()) });
		db.close();
	}

}
