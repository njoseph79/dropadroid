package com.mrm.dropadroid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ArrayAdapter;

public class ArticleOverview extends Activity {
	private static final String CLASSID = "ArticleOverview: ";
	private int bookid;
	private DatabaseHandler db;
	private int[] articleIds;
	public final static String SELECTED_BOOK_ID = "SELECTED_BOOK_ID";
	public final static String SELECTED_ARTICLE_ID = "SELECTED_ARTICLE_ID";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_article_overview);
		
		this.db = new DatabaseHandler(this);
	}
	@Override
	protected void onStart(){
		super.onStart();
		this.updateView();
	}

	private void updateView() { 
		Bundle extras = getIntent().getExtras();
		List<Article> articles = null;

	    final ArrayList<String> list = new ArrayList<String>();
	    
		
		if (extras != null) {
			this.bookid = extras.getInt(SELECTED_BOOK_ID);
			Log.d(CLASSID + "load articles for book: ", Integer.toString(this.bookid));

			// Reading all articles
			articles = this.db.getArticlesForBook(this.bookid);
		    this.articleIds = new int[articles.size()];
		    int i = 0;
			for (Iterator<Article> iterator = articles.iterator(); iterator
					.hasNext();) {
				Article anArticle = (Article) iterator.next();
				// do some stuff
				Log.d(CLASSID + "articles title: ", anArticle.getTitle());
				list.add(anArticle.getTitle());
				this.articleIds[i] = anArticle.getID();
				i++;

			}
		}
		else {
			Log.d(CLASSID + "articles extras is null!! ","");
			
		}
		final List<Article> items = articles;
		final ListView listview = (ListView) findViewById(R.id.listview);
		// table.setBackgroundResource(R.drawable.bg_overview);

		final StableArrayAdapter adapter = new StableArrayAdapter(this,
				android.R.layout.simple_list_item_1, items, list);
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
				final String item = (String) parent.getItemAtPosition(position);
				Log.d(CLASSID + "article " + Integer.toString(position) + " clicked: ", item);
				Intent intent = new Intent("com.mrm.dropadroid.ArticleDetailview");
				intent.putExtra(SELECTED_ARTICLE_ID, ArticleOverview.this.articleIds[position]);
				startActivity(intent);
				/*
				view.animate().setDuration(2000).alpha(0).withEndAction(new Runnable() {
							@Override
							public void run() {
								//items.remove(item);
								adapter.notifyDataSetChanged();
								view.setAlpha(1);
							}
						});
						*/
			}

		});
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.article_overview, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.action_create_article:
			// create new Article
			Article anArticle = new Article("", this.bookid);
			anArticle.setTitle("");
			// fetch article id
			int id = this.db.addArticle(anArticle);
			Log.d(CLASSID + "Create Article: ", Integer.toString(id));
			// intent article detail acitity
			Intent intent = new Intent("com.mrm.dropadroid.ArticleDetailview");
			intent.putExtra(SELECTED_ARTICLE_ID, id);
			startActivity(intent);
			
			return true;
		case R.id.action_edit_articles:
			Log.d(CLASSID + "Edit Book: ", "..");
			// make article rows somehow selectable, to perform multiple deletions
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	

	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<Article> objects, List<String> titles) {
			super(context, textViewResourceId, titles);
			for (int i = 0; i < titles.size(); ++i) {
				mIdMap.put(titles.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}
}
