package com.mrm.dropadroid;

import java.util.Iterator;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ScrollView;
import android.widget.FrameLayout.LayoutParams;

public class BookOverview extends Activity implements
		android.content.DialogInterface.OnClickListener {
	private int alertType;
	private static final int CREATEID = 1;
	private static final int EDITID = 2;
	private static final int DELETEID = 3;
	private static final String CLASSID = "BookOverview: ";
	private EditText titleInput;
	private AlertDialog.Builder createDialog;
	private AlertDialog.Builder editDialog;
	private AlertDialog.Builder deleteDialog;
	private BookSelector bs;
	private List<Book> books;
	public final static String SELECTED_BOOK_ID = "SELECTED_BOOK_ID";

	public void bookSelected(int bookiId) {
		Log.d(CLASSID + "bookSelected: ", Integer.toString(bookiId));
		Intent intent = new Intent(this, ArticleOverview.class);
		intent.putExtra(SELECTED_BOOK_ID, bookiId);
		startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_book_overview);
		this.alertType = 0;
		this.updateView();
	}

	private void updateView() {
		DatabaseHandler db = new DatabaseHandler(this);
		/*
		 * TESTDATA BLOCK
		 */
		/*
		 * Log.d(CLASSID + "Inserting: ", "Inserting ..");
		 * 
		 * db.addBook(new Book("Droid Book")); db.addArticle(new Article(
		 * "Vor langer langer Zeit in einem kleinen kleinen Wald lebte ein Kind das gerne mit Knochen spielte"
		 * , 1)); db.addArticle(new Article( "Fischers Fritze fischt frische",
		 * 1)); db.addArticle(new Article(
		 * "Palim Palim, eine Notiz ist besser als die Taube auf dem Dach", 1));
		 * 
		 * 
		 * db.addBook(new Book("Another Book")); db.addArticle(new Article(
		 * "Fischers Fritze fischt frische", 2));
		 * 
		 * 
		 * db.addBook(new Book("Private")); db.addArticle(new Article(
		 * "Liebes Tagebuch, heute ist ein schöner Tag denn mein Hamster ist wieder gesund und wohl auf in seinem kleinen Hamsterrad."
		 * , 3));
		 */
		// Reading all books
		Log.d(CLASSID + "Read: ", "Read all fff books..");
		this.books = db.getAllBooks();

		this.bs = new BookSelector(this);
		this.bs.setFeatureItems(this.books);
		this.bs.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		this.bs.setBookOverviewObject(this);

		ViewGroup mainView = (ViewGroup) getLayoutInflater().inflate(
				R.layout.activity_book_overview, null);
		mainView.addView(this.bs);
		this.setContentView(mainView);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.book_overview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.action_create_book:
			Log.d(CLASSID + "Create Book: ", "..");
			this.showBookCreationDialog();
			return true;
		case R.id.action_edit_book:
			Log.d(CLASSID + "Edit Book: ", "..");
			this.showBookEditDialog();
			return true;
		case R.id.action_delete_book:
			Log.d(CLASSID + "Delete Book: ", "..");
			this.showBookDeleteDialog();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void showBookCreationDialog() {
		createDialog = new AlertDialog.Builder(this);
		this.alertType = CREATEID;
		createDialog.setTitle("Create a new book");
		createDialog.setMessage("Please enter a valid book title");

		// Set an EditText view to get user input
		// final EditText input = new EditText(this);
		this.titleInput = new EditText(this);
		createDialog.setView(titleInput);
		createDialog.setPositiveButton("Ok", this);
		createDialog.setNegativeButton("Cancel", this);
		/*
		 * alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		 * public void onClick(DialogInterface dialog, int whichButton) {
		 * Editable value = input.getText(); // Do something with value!
		 * Log.d(CLASSID + " CREATE NEW BOOK WITH TITLE :  " ,
		 * value.toString()); DatabaseHandler db = new
		 * DatabaseHandler(BookOverview.this); Book newBook = new Book();
		 * newBook.setTitle(value.toString()); db.addBook(newBook);
		 * 
		 * } }); /* alert.setNegativeButton("Cancel", new
		 * DialogInterface.OnClickListener() { public void
		 * onClick(DialogInterface dialog, int whichButton) { // Canceled. } });
		 */
		createDialog.show();
	}

	private void showBookEditDialog() {
		editDialog = new AlertDialog.Builder(this);
		this.alertType = EDITID;
		editDialog.setTitle("Edit book title");
		editDialog.setMessage("Please enter a valid book title");
		// Set an EditText view to get user input
		// final EditText input = new EditText(this);
		this.titleInput = new EditText(this);
		editDialog.setView(titleInput);
		editDialog.setPositiveButton("Ok", this);
		editDialog.setNegativeButton("Cancel", this);

		editDialog.show();
	}
	
	private void showBookDeleteDialog() {
		deleteDialog = new AlertDialog.Builder(this);
		this.alertType = DELETEID;
		deleteDialog.setTitle("Delete current book");
		deleteDialog.setMessage("Do you really want to delete the current book?");

		deleteDialog.setPositiveButton("Ok", this);
		deleteDialog.setNegativeButton("Cancel", this);

		deleteDialog.show();
	}

	@Override
	public void onClick(DialogInterface dialog, int whichButton) {

		Log.d(CLASSID + " onClick:  ", Integer.toString(this.alertType));
		DatabaseHandler db = new DatabaseHandler(this);
		if (this.alertType == CREATEID) {

			Log.d(CLASSID + " createDialog:  ", Integer.toString(whichButton));
			switch (whichButton) {
			case -1:
				Editable value = this.titleInput.getText();
				// Do something with value!
				Log.d(CLASSID + " CREATE NEW BOOK WITH TITLE :  ",
						value.toString());
				Book newBook = new Book();
				newBook.setTitle(value.toString());
				db.addBook(newBook);
				this.updateView();
			default:

			}
		} else if (this.alertType == EDITID || this.alertType == DELETEID) {
			Log.d(CLASSID + " updateDialog:  ", Integer.toString(whichButton));
			// Do something with value!
			int i = 0;
			Book current = null;
			for (Iterator<Book> iterator = this.books.iterator(); iterator
					.hasNext();) {
				current = iterator.next();
				if (i == this.bs.getmActiveFeature()) {

					Log.d(CLASSID + " UPDATE BOOK WITH TITLE :  ",
							current.getTitle());
					break;
				}
				i++;
			}
			if (null != current) {
				if (this.alertType == DELETEID) {
					db.deleteBook(current);
				} else {
					Editable value = this.titleInput.getText();
					current.setTitle(value.toString());
					db.updateBook(current);
				}
				this.updateView();
			}
		}
	}
}
