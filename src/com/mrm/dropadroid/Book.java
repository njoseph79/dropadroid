package com.mrm.dropadroid;

public final class Book {
	int _id;
	String _title;
	String _created;
	String _modified;

	/**
	 * Default Constructor
	 */
	public Book() {

	}

	/**
	 * Constructor
	 */
	public Book(int id, String title, String created, String modified) {
		this._id = id;
		this._title = title;
		this._created = created;
		this._modified = modified;
	}

	/**
	 * Constructor
	 */
	public Book(String title) {
		this._title = title;
	}

	/**
	 * getters
	 */
	public int getID() {
		return this._id;
	}

	public String getTitle() {
		return this._title;
	}

	public String getCreated() {
		return this._created;
	}

	public String getModified() {
		return this._modified;
	}

	/**
	 * setters
	 */
	public void setID(int id) {
		this._id = id;
	}

	public void setTitle(String title) {
		this._title = title;
	}

	public void setCreated(String created) {
		this._created = created;
	}

	public void setModified(String modified) {
		this._modified = modified;
	}
}
