package com.mrm.dropadroid;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;

public class BookSelector extends HorizontalScrollView implements
		OnClickListener {

	private static final String CLASSID = "BookSelector: ";
	private static final int SWIPE_MIN_DISTANCE = 5;
	private static final int SWIPE_THRESHOLD_VELOCITY = 300;
	private List<Book> mItems = null;
	private GestureDetector mGestureDetector;
	private Context mContext;
	public int mActiveFeature = 0;
	public int getmActiveFeature() {
		return mActiveFeature;
	}

	public void setmActiveFeature(int mActiveFeature) {
		this.mActiveFeature = mActiveFeature;
	}

	private BookOverview bOverview = null;

	public BookSelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.mContext = context;
	}

	public BookSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
	}

	public BookSelector(Context context, List<Book> items) {
		super(context);
		this.mContext = context;
		mItems = items;
	}

	public BookSelector(Context context) {
		super(context);
		this.mContext = context;
	}

	public void setBookOverviewObject(BookOverview object) {
		this.bOverview = object;
	}

	public void setFeatureItems(List<Book> items) {
		// Create a linear layout to hold each screen in the scroll view
		LinearLayout internalWrapper = new LinearLayout(getContext());
		WindowManager wm = (WindowManager) this.mContext
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;

		internalWrapper.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		internalWrapper.setOrientation(LinearLayout.HORIZONTAL);
		addView(internalWrapper);
		this.mItems = items;

		Drawable bookCover = getResources()
				.getDrawable(R.drawable.book_default);
		int h = bookCover.getIntrinsicHeight();
		int w = bookCover.getIntrinsicWidth();

		for (Book aBook : mItems) {

			int paddingLR = (width / 2) - (w / 2);

			ImageButton aButton = new ImageButton(this.mContext);
			aButton.setImageResource(R.drawable.book_default);
			aButton.setBackgroundColor(Color.argb(0, 0, 0, 0));
			aButton.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT));
			aButton.setPadding(paddingLR, 0, paddingLR, 0);
			aButton.setId(aBook.getID());
			aButton.setOnClickListener(this);

			String log = CLASSID + "Id: " + aBook.getID() + " ,Title: "
					+ aBook.getTitle() + " ,created: " + aBook.getCreated()
					+ "width: " + width + "height: " + height + "buttonwidth: "
					+ w;
			// Writing Contacts to log
			Log.d(CLASSID + "bookinfo: ", log);

			TextView coverText = new TextView(getContext());
			coverText.setTextColor(Color.argb(255, 55, 55, 55));
			coverText.setTextSize(18.0f);
			coverText.setText(aBook.getTitle());
			coverText.setPadding(paddingLR + (w / 8), (height / 2)
					- (height / 16), paddingLR, 0);

			RelativeLayout rl = new RelativeLayout(getContext());
			rl.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT));
			rl.addView(aButton);
			rl.addView(coverText);
			// rl.setBackgroundColor(Color.argb(,255,0,0));

			internalWrapper.addView(rl);
		}

		// Set the touch listener for handling a slow drag and release
		setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// If the user swipes
				if (mGestureDetector.onTouchEvent(event)) {
					return true;
				} else if (event.getAction() == MotionEvent.ACTION_UP
						|| event.getAction() == MotionEvent.ACTION_CANCEL) {
					int scrollX = getScrollX();
					int featureWidth = v.getMeasuredWidth();
					mActiveFeature = ((scrollX + (featureWidth / 2)) / featureWidth);
					int scrollTo = mActiveFeature * featureWidth;
					smoothScrollTo(scrollTo, 0);
					return true;
				} else {
					return false;
				}
			}
		});
		mGestureDetector = new GestureDetector(new MyGestureDetector());
	}

	class MyGestureDetector extends SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {
				// right to left
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					int featureWidth = getMeasuredWidth();
					mActiveFeature = (mActiveFeature < (mItems.size() - 1)) ? mActiveFeature + 1
							: mItems.size() - 1;
					smoothScrollTo(mActiveFeature * featureWidth, 0);
					return true;
				}
				// left to right
				else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					int featureWidth = getMeasuredWidth();
					mActiveFeature = (mActiveFeature > 0) ? mActiveFeature - 1
							: 0;
					smoothScrollTo(mActiveFeature * featureWidth, 0);
					return true;
				}
			} catch (Exception e) {
				Log.e("Fling", "There was an error processing the Fling event:"
						+ e.getMessage());
			}
			return false;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		this.bOverview.bookSelected(v.getId());
	}

}
