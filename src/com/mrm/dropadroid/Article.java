package com.mrm.dropadroid;

import android.util.Log;

public class Article {
	int _id;
	int _bookId;
	String _title;
	String _text;
	String _created;
	String _modified;

	/**
	 * Default Constructor
	 */
	public Article() {

	}

	/**
	 * Constructor
	 */
	public Article(int id, int bookId, String title, String text,
			String created, String modified) {
		this._id = id;
		this._bookId = bookId;
		this._title = title;
		this._text = text;
		this._created = created;
		this._modified = modified;
	}

	/**
	 * Constructor
	 */
	public Article(String text, int bookId) {

		this._bookId = bookId;
		this._text = text;
		String title = new String();
		title = text;
		if (title.length() > 32) {
			title = title.substring(0, 32);
		}
		this._title = title;
	}

	/**
	 * getters
	 */
	public int getID() {
		return this._id;
	}

	public int getBookID() {
		return this._bookId;
	}

	public String getTitle() {
		if (this._title.length() > 32) {
			this._title = this._title.substring(0, 32);
		}
		return this._title;
	}

	public String getText() {
		return this._text;
	}

	public String getCreated() {
		return this._created;
	}

	public String getModified() {
		return this._modified;
	}

	/**
	 * setters
	 */
	public void setID(int id) {
		this._id = id;
	}

	public void setBookID(int id) {
		this._bookId = id;
	}

	public void setText(String text) {
		this._text = text;
	}

	public void setTitle(String title) {
		this._title = title;
	}

	public void setCreated(String created) {
		this._created = created;
	}

	public void setModified(String modified) {
		this._modified = modified;
	}
}
